﻿using Assignment1_RPGChars.Equipment;
using Assignment1_RPGChars.CustomExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPGChars.Hero
{

    struct Attributes
    {
        private int Strength;
        private int Dexterity;
        private int Intelligence;

        /// <summary>
        /// Gets the current Attributes instance.
        /// </summary>
        /// <returns>
        /// Datatype Attributes.
        /// </returns>
        public Attributes GetAttributes()
        {
            return this;
        }

        /// <summary>
        /// Overloaded function of GetAttributes. Takes a string input that should correspond to the 
        ///     attribute: Strength, Dexterity or Intelligence. 
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns>
        /// The value of the specified attribute as an Int.
        /// </returns>
        public int GetAttributes(string attribute)
        {
            int value = 0;
            switch (attribute)
            {
                case "Strength":
                    value = Strength;
                    break;

                case "Dexterity":
                    value = Dexterity;
                    break;

                case "Intelligence":
                    value = Intelligence;
                    break;

                default:
                    break;
            }

            return value;
        }

        /// <summary>
        /// Sets the current Attributes instance private members to the input arguments.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="dex"></param>
        /// <param name="intel"></param>
        public void SetAttributes(int str, int dex, int intel)
        {
            Strength += str;
            Dexterity += dex;
            Intelligence += intel;
        }

        /// <summary>
        /// Overloaded SetAttributes function. Sets the current private members to the 
        /// input Attributes object.
        /// </summary>
        /// <param name="other"></param>
        public void SetAttributes(Attributes other)
        {
            Strength += other.Strength;
            Dexterity += other.Dexterity;
            Intelligence += other.Intelligence;
        }

        /// <summary>
        /// Takes in two objects of type Attributes and adds the private members together.
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="other"></param>
        /// <returns>
        /// Returns a new Attribute object with the added values.
        /// </returns>
        public Attributes CalculateTotalAttributes(Attributes origin, Attributes other)
        {
            Attributes temp = new Attributes();
            temp.Strength = origin.Strength + other.Strength;
            temp.Dexterity = origin.Dexterity + other.Dexterity;
            temp.Intelligence = origin.Intelligence + other.Intelligence;

            return temp;
        }
    }
    abstract class Character
    {

        private string charName;
        private int charLevel;
        private Attributes basePrimaryAtt;
        private Attributes totPrimaryAtt;
        private List<WeaponType> allowedWeapons;
        private List<ArmourType> allowedArmour;
        private float characterDamage;

        protected Dictionary<Slot, Item> equipment = new Dictionary<Slot, Item> { };

        // -------------------- Constructors & Get/Set ---------------------- //
        public Character()
        {
            charName = "Character";
            charLevel = 1;
            basePrimaryAtt = new Attributes();
            basePrimaryAtt.SetAttributes(1, 1, 1);
            totPrimaryAtt.SetAttributes(0, 0, 0);
            allowedWeapons = new List<WeaponType>();
            allowedArmour = new List<ArmourType>();
        }

        /// <summary>
        /// Character constructer overload. Takes in a string value for the Character name.
        /// </summary>
        /// <param name="charName"></param>
        public Character(string charName)
        {
            this.charName = charName;
            charLevel = 1;
            basePrimaryAtt = new Attributes();
            basePrimaryAtt.SetAttributes(1, 1, 1);
            totPrimaryAtt.SetAttributes(0, 0, 0);
            allowedWeapons = new List<WeaponType>();
            allowedArmour = new List<ArmourType>();
        }

        public string CHaracterName
        {
            get { return charName; }
            set { charName = value; }
        }
        public int CharacterLevel
        {
            get { return charLevel; }
            set { charLevel = value; }
        }

        // ----------------- Attributes --------------------- //

        /// <summary>
        /// Sets the Base Primary Attributes via three integer arguments. Strength, Dexterity and Intelligence.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="dex"></param>
        /// <param name="intel"></param>
        protected void SetBasePrimaryAttributeIncrease(int str, int dex, int intel)
        {
            basePrimaryAtt.SetAttributes(str, dex, intel);
        }

        /// <summary>
        /// Calculates the total primary attributes by adding together the base primary attributes and another Attributes object.
        /// Proceeds to update the current base primary attribute with 1. This is done because it will only be called once a new Armour object is added.
        /// </summary>
        /// <param name="other"></param>
        public void TotalAttributes(Attributes other)
        {
            totPrimaryAtt = totPrimaryAtt.CalculateTotalAttributes(basePrimaryAtt, other);
            basePrimaryAtt.SetAttributes(1, 1, 1);
        }

        /// <summary>
        /// Abstract function that will add the appropriate amount of attributes depending on the child class.
        /// </summary>
        public abstract void LevelUp();

        /// <summary>
        /// Takes in the damage from the class object in order to calculate the final Character Damage.
        /// As the equipment dictionary is filled with Item objects a TryGetValue function is called to assess if there is a Weapon Slot type. 
        /// If the value is returned as 'null' it is assumed that the Character has no wepon equipped resulting in a DPS value of 1.
        /// If the value is indeed of a Weapon Slot type the DPS value is fetched via the GetData function.
        /// </summary>
        /// <param name="classDmg"></param>
        public void CharacterDamage(float classDmg)
        {
            Item temp;
            float DPS = 0;
            equipment.TryGetValue(Slot.WEAPON, out temp);
            if(temp != null)
            {
                DPS = temp.GetData<float>();
            }
            else
            {
                DPS = 1;
            }
            characterDamage = DPS * classDmg;
        }

        /// <summary>
        /// Gets the specified value from the Attributes object via a string input.
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns>
        /// Returns the Attributes private member value as an int.</returns>
        protected int GetTotAttribute(string attribute)
        {
            return totPrimaryAtt.GetAttributes(attribute);
        }

        /// <summary>
        /// Creates a String Builder object abd appends data about the Character object.
        /// </summary>
        public void CharacterSheet()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Name: " + CHaracterName);
            sb.AppendLine("Level: " + CharacterLevel.ToString());
            sb.AppendLine("Strength: " + totPrimaryAtt.GetAttributes("Strength"));
            sb.AppendLine("Dexterity: " + totPrimaryAtt.GetAttributes("Dexterity"));
            sb.AppendLine("Intelligence: " + totPrimaryAtt.GetAttributes("Intelligence"));
            sb.AppendLine("Damage: " + characterDamage.ToString());

            Console.WriteLine(sb);
        }


        // ------------------- Item Section ----------------- //
        /// <summary>
        /// Prints the obejct values in the equipment dictionary.
        /// </summary>
        protected void PrintEquipment()
        {
            foreach (var item in equipment)
            {
                Console.WriteLine("EQUIP: " /*+ item.Key*/ + item.Value.PrintInfoItem());
            }
        }

        /// <summary>
        /// Attempts to throw an Exception if the current character level doesn't match the weapon level requirement.
        /// If the requirement is met a new Weapon object is created based on the parameters. 
        /// The Replace Item function removes if a similar Slot-type already exists in the Dictionary. As weapons can only be in one slot it is hardcoded.
        /// Adds the new Weapon object to the Dictionary.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="requiredLevel"></param>
        /// <param name="weapType"></param>
        /// <param name="baseDmg"></param>
        /// <param name="attPerSec"></param>
        public void AddWeapon(string name, int requiredLevel, WeaponType weapType, int baseDmg, int attPerSec)
        {
            try
            {
                MyCustomException.TooLowLevelException(CharacterLevel, requiredLevel);

                Weapon newWeap = new Weapon(name, requiredLevel, weapType, baseDmg, attPerSec);

                ReplaceItem(Slot.WEAPON);
                equipment.Add(Slot.WEAPON, newWeap);
            }
            catch(MyCustomException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Attempts to throw an Exception if the current character level doesn't match the armour level requirement.
        /// If the requirement is met a new Armour object is created based on the parameters. 
        /// The Replace Item function removes if a similar Slot-type already exists in the Dictionary.
        /// Adds the new Armour object to the Dictionary.
        /// Calculates the Total Attributes by calling a function in the Attributes struct.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="requiredLevel"></param>
        /// <param name="slotType"></param>
        /// <param name="armourType"></param>
        public void AddArmour(string name, int requiredLevel, Slot slotType, ArmourType armourType)
        {
            try
            {
                MyCustomException.TooLowLevelException(CharacterLevel, requiredLevel);

                Armour newArmour = new Armour(name, requiredLevel, slotType, armourType);

                ReplaceItem(slotType);
                equipment.Add(slotType, newArmour);
                TotalAttributes(newArmour.PrimaryAttributes);
            }
            catch(MyCustomException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// ReplaceItem loops through the equipment dictionary and removes 
        /// an item that has the same Slot-type as the input. As only ONE type 
        /// of object for every Slot is allowed this function works.
        /// </summary>
        /// <param name="slotType"></param>
        private void ReplaceItem(Slot slotType)
        {
            foreach (var item in equipment)
            {
                if (slotType == item.Key)
                {
                    equipment.Remove(item.Key);
                }
            }
        }

        /// <summary>
        /// Adds the specified WeaponType object to the list of allowed weapon types 
        /// for the instantiated Character object.
        /// </summary>
        /// <param name="weapon"></param>
        protected void AddToAllowedWeaponList(WeaponType weapon)
        {
            allowedWeapons.Add(weapon);
        }

        /// <summary>
        /// Adds the specified ArmourType object to the list of allowed armour types 
        ///for the instantiated Character object.
        /// </summary>
        /// <param name="armour"></param>
        protected void AddToAllowedArmourList(ArmourType armour)
        {
            allowedArmour.Add(armour);
        }

        // -------------------- Exceptions --------------------- //

        /// <summary>
        /// Throws a Exception if the input WeaponType object does not exists in the 
        /// allowed weapon list.
        /// </summary>
        /// <param name="weaponInput"></param>
        /// <exception cref="MyCustomException"></exception>
        protected virtual void InvalidWeaponException(WeaponType weaponInput)
        {
            
            if(!allowedWeapons.Contains(weaponInput))
            {
                throw new MyCustomException("That WEAPON Type Is Not Valid For Your Character-Class!");
            }
            
        }

        /// <summary>
        /// Throws an Exception if the given ArmourType object does not exist in the 
        /// allowed aremour list.
        /// </summary>
        /// <param name="armourInput"></param>
        /// <exception cref="MyCustomException"></exception>
        protected virtual void InvalidArmourException(ArmourType armourInput)
        {

            if (!allowedArmour.Contains(armourInput))
            {
                throw new MyCustomException("That ARMOUR Type Is Not Valid For Your Character-Class!");
            }

        }


    }
}

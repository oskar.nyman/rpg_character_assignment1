﻿// See https://aka.ms/new-console-template for more information

using Assignment1_RPGChars.Hero;
using Assignment1_RPGChars.Equipment;

//Console.WriteLine("Hello, World!");

Mage test = new Mage("Khadgar");

Console.WriteLine("**********");
test.LevelUp();
// Adding Weapons
test.AddNewWeapon("Staff of Mediev", 2, WeaponType.STAFF, 5, 2);

// Adding Armour
test.AddNewArmour("Loin Cloth", 1, Slot.LEGS, ArmourType.CLOTH);
test.AddNewArmour("Cap of Dumbness", 2, Slot.HEAD, ArmourType.MAIL);

test.CurrentEquipment();
Console.WriteLine("**********");
test.AddNewWeapon("Elder Wand", 2, WeaponType.SWORD, 5, 2);
test.CurrentEquipment();
test.CharacterDamageFinal();

Console.WriteLine("**********");

test.CharacterSheet();
Console.WriteLine("**********");
Console.WriteLine("**********");

Warrior test2 = new Warrior("Garrosh");

Console.WriteLine("**********");
test2.LevelUp();
test2.LevelUp();
// Adding Weapons
test2.AddNewWeapon("Gorehowl", 2, WeaponType.AXE, 17, 1);

// Adding Armour
test2.AddNewArmour("Loin Cloth", 1, Slot.LEGS, ArmourType.CLOTH);
test2.AddNewArmour("Helm of Fools", 3, Slot.HEAD, ArmourType.MAIL);
test2.AddNewArmour("Leggings of Fools", 2, Slot.LEGS, ArmourType.PLATE);

test2.CurrentEquipment();
Console.WriteLine("**********");
test2.AddNewWeapon("Magic Sword", 1, WeaponType.SWORD, 5, 2);
test2.CurrentEquipment();
test2.CharacterDamageFinal();

Console.WriteLine("**********");

test2.CharacterSheet();
Console.WriteLine("**********");
Console.WriteLine("**********");
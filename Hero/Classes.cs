﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment1_RPGChars.Equipment;
using Assignment1_RPGChars.CustomExceptions;

namespace Assignment1_RPGChars.Hero
{
    class Mage : Character
    {
        /// <summary>
        /// Calls the base constructor and sets the name of the object. 
        /// Sets the base primary attributes to the Mage appropriate attributes.
        /// Updates the allowed weapon and armour lists which can be used to check if a certain weapon/armour can be equipped on Mage.
        /// </summary>
        /// <param name="name"></param>
        public Mage(string name)
            : base(name)
        {
            SetBasePrimaryAttributeIncrease(0, 0, 7);

            AddToAllowedWeaponList(WeaponType.STAFF);
            AddToAllowedWeaponList(WeaponType.WAND);

            AddToAllowedArmourList(ArmourType.CLOTH);
        }
        /// <summary>
        /// Increments the chatacter level by one. Sets the base primary attributes to increase by the hardcoded amount.
        /// </summary>
        public override void LevelUp()
        {
            CharacterLevel++;
            SetBasePrimaryAttributeIncrease(1, 1, 5);
        }

        // ------------------ Items ------------------- //
        /// <summary>
        /// Attempts to throw an InvalidWeaponException based in the weapon type and the allowed weapons list.
        /// If there is no exception thrown a new weapon will be added to the equipment list. Base class AddWeapon function takes care of this.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="requiredLevel"></param>
        /// <param name="weapType"></param>
        /// <param name="baseDmg"></param>
        /// <param name="attPerSec"></param>
        public void AddNewWeapon(string name, int requiredLevel, WeaponType weapType, int baseDmg, int attPerSec)
        {
            try
            {
                InvalidWeaponException(weapType);
                AddWeapon(name, requiredLevel, weapType, baseDmg, attPerSec);
            }
            catch (MyCustomException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        /// <summary>
        /// Attempts to throw an InvalidArmourException based in the armour type and the allowed armour list.
        /// If there is no exception thrown a new armour will be added to the equipment list. Base class AddArmour function takes care of this.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="requiredLevel"></param>
        /// <param name="slotType"></param>
        /// <param name="armourType"></param>
        public void AddNewArmour(string name, int requiredLevel, Slot slotType, ArmourType armourType)
        {
            try
            {
                InvalidArmourException(armourType);
                AddArmour(name, requiredLevel, slotType, armourType);
            }
            catch (MyCustomException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        /// <summary>
        /// Prints the current equipment with the help of base class PrintEquipment function.
        /// </summary>
        public void CurrentEquipment()
        {
            PrintEquipment();
        }
        /// <summary>
        /// Calculates the damage of the class based on primary attribute for each class, Intelligence for Mage.
        /// Calls base class CharacterDamage function and supplies it with the calculated class damage.
        /// </summary>
        public void CharacterDamageFinal()
        {
            float classDmg = 1 + (float)GetTotAttribute("Intelligence") / 100;
            //Console.WriteLine(GetTotAttribute("Intelligence"));
            CharacterDamage(classDmg);
            //return (DPS * (1 + GetTotAttribute("Intelligence") / 100));
        }
    }
}

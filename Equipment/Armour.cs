﻿using Assignment1_RPGChars.Hero;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPGChars.Equipment
{
    enum ArmourType
    {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }

    class Armour : Item
    {
        private ArmourType armour;
        private Attributes primaryAttributes;

        /// <summary>
        /// Calls base and sets values to private members according to the arguments. Creates a new Attributes object. Sets the attributes to 1.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="requiredLevel"></param>
        /// <param name="slotType"></param>
        /// <param name="armour"></param>
        public Armour(string name, int requiredLevel, Slot slotType, ArmourType armour)
            : base(name, requiredLevel, slotType)
        {
            this.armour = armour;
            primaryAttributes = new Attributes();
            primaryAttributes.SetAttributes(1, 1, 1);
        }

        /// <summary>
        /// Gets amd returns the Attributes object.
        /// </summary>
        public Attributes PrimaryAttributes { get { return primaryAttributes; } }

        // CONSIDER REMOVING
        /// <summary>
        /// Gets the armour object along with the name.
        /// </summary>
        /// <returns>
        /// String value of armour object and name. </returns>
        public override string PrintInfoItem()
        {
            return "Armour: " + armour.ToString() + GetName();
        }

        /// <summary>
        /// Redundant function as it was intended to return certain data. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>
        /// No longer in use.</returns>
        public override T GetData<T>()
        {
            return (T)Convert.ChangeType(1, typeof(T));
        }

    }
}

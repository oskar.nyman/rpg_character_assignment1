﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment1_RPGChars.Equipment;
using Assignment1_RPGChars.CustomExceptions;

namespace Assignment1_RPGChars.Hero
{
    class Warrior : Character
    {
        public Warrior(string name)
           : base(name)
        {
            SetBasePrimaryAttributeIncrease(4, 1, 0);

            AddToAllowedWeaponList(WeaponType.AXE);
            AddToAllowedWeaponList(WeaponType.HAMMER);
            AddToAllowedWeaponList(WeaponType.SWORD);

            AddToAllowedArmourList(ArmourType.MAIL);
            AddToAllowedArmourList(ArmourType.PLATE);
        }
        public override void LevelUp()
        {
            CharacterLevel++;
            SetBasePrimaryAttributeIncrease(3, 2, 1);
        }

        public void AddNewWeapon(string name, int requiredLevel, WeaponType weapType, int baseDmg, int attPerSec)
        {
            try
            {
                InvalidWeaponException(weapType);
                AddWeapon(name, requiredLevel, weapType, baseDmg, attPerSec);
            }
            catch (MyCustomException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void AddNewArmour(string name, int requiredLevel, Slot slotType, ArmourType armourType)
        {
            try
            {
                InvalidArmourException(armourType);
                AddArmour(name, requiredLevel, slotType, armourType);
            }
            catch (MyCustomException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void CurrentEquipment()
        {
            PrintEquipment();
        }

        /// <summary>
        /// Calculates the damage of the class based on primary attribute for each class, Srength for Warrior.
        /// Calls base class CharacterDamage function and supplies it with the calculated class damage.
        /// </summary>
        public void CharacterDamageFinal()
        {
            float classDmg = 1 + (float)GetTotAttribute("Strength") / 100;
            CharacterDamage(classDmg);
        }
    }
}

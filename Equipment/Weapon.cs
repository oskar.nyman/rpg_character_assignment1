﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPGChars.Equipment
{
    enum WeaponType
    {
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND
    }

    class Weapon : Item
    {
        private int baseDmg;
        private int attacksPerSecond;
        private float dps;
        private WeaponType type;

        /// <summary>
        /// Calls base and sets values for the private member variables, hardcodes Weapon slot as there is only one such Slot.
        /// Calculates the DPS for the weapon object and sets the type based on the argument.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="requiredLevel"></param>
        /// <param name="weapon"></param>
        /// <param name="baseDmg"></param>
        /// <param name="attPerSec"></param>
        public Weapon(string name, int requiredLevel, WeaponType weapon, int baseDmg, int attPerSec)
            : base(name, requiredLevel, Slot.WEAPON)
        {
            this.baseDmg = baseDmg;
            attacksPerSecond = attPerSec;
            dps = this.baseDmg * attacksPerSecond;
            type = weapon;
        }

        /// <summary>
        /// Gets the weapon object DPS.
        /// </summary>
        /// <returns>The DPS as a float.</returns>
        public float GetDPS() { return dps; }
        /// <summary>
        /// Gets the type of weapon and dps.
        /// </summary>
        /// <returns>A string value for the weapon type and dps.</returns>
        public override string PrintInfoItem()
        {
            return "Weapon Type: " + type.ToString() + " DPS: " + GetDPS().ToString();
        }
        /// <summary>
        /// Gets the DPS of the weapon object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>DPS depending on the datatype. In this case returns DPS as a float value.</returns>
        public override T GetData<T>()
        {
            return (T)Convert.ChangeType(GetDPS(), typeof(T));
        }
    }
}

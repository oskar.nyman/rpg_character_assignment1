﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPGChars.CustomExceptions
{
    public class MyCustomException : Exception
    {
        public MyCustomException(string? message) : base(message)
        {
        }

        /// <summary>
        /// Takes in two integer values and compares them based on value. If the current level is lower than the item requirement an exception is thrown.
        /// </summary>
        /// <param name="currentLevel"></param>
        /// <param name="itemLevel"></param>
        /// <exception cref="MyCustomException"></exception>
        public static void TooLowLevelException(int currentLevel, int itemLevel)
        {
            if(currentLevel < itemLevel)
            {
                throw new MyCustomException("Your Character Is Too Low Level For That Item!");
            }
        }
        public override string Message => base.Message;
    }
}

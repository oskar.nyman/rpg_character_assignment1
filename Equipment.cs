﻿using Assignment1_RPGChars.Items;

namespace Assignment1_RPGChars
{
    class Equipment
    {
        private List<Item> items;

        Equipment()
        {
            items = new List<Item>();
        }

        public void AddItem(Item item) {
            items.Add(item);
        }

        public void RemoveItem(Item item) { 
            items.Remove(item);
        }

        public int GetSize() { return items.Count; } 

        public Item AtIndex(int i) { return items[i]; }  
    }
}
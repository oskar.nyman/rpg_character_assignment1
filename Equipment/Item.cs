﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPGChars.Equipment
{
    // An enumerator that will aid the user to specify which Item Slot an Item object can be placed in.
    enum Slot
    {
        HEAD,
        BODY,
        LEGS,
        WEAPON,
        EMPTY
    }

    // Abstract Parent Item class due to only child class objects will be instantiated
    abstract class Item
    {
        private string name;
        private int requiredLevel;
        private Slot slot;

        // -------------------- Constructors ------------------- //
        Item()
        {
            name = "Wabbajack";
            requiredLevel = 1;
            slot = Slot.EMPTY;
        }
        protected Item(string name, int requiredLevel, Slot slot)
        {
            this.name = name;
            this.requiredLevel = requiredLevel;
            this.slot = slot;
        }

        // -------------------- Get/Set ----------------------- //

        // CONSIDER REMOVING
        protected virtual string GetName() { return name; }
        /// <summary>
        /// Abstract function that will get data of any type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>Data depending on the datatype.</returns>
        public abstract T GetData<T>();
        /// <summary>
        /// Prints data such as item name, required level and slot type.
        /// </summary>
        /// <returns>A string value with name, required level and slot.</returns>
        public virtual string PrintInfoItem() { return "Item name: " + name + " Required Level: " + requiredLevel + " Slot: " + slot; }

    }
}
